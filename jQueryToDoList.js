$(document).ready(function() {
  /**
 * Toggles "done" class on <li> element
 */
$('li').on('click', function(e) {
  let $this = $(this);
  $(this).toggleClass('done');
});
/**
 * Delete element when delete link clicked
 */
$('.delete').on('click', function () {
  let $this = $(this);
  $(this).closest('li').fadeOut(function () {
    $(this).closest('li').remove();
  }); 
});

$('.add-item').on('click', function(e) {
  e.preventDefault();
  const text = $('input').val(); 
  const element = $('<li><span>' + text + '</span><a class="delete">Delete</a></li>');
  const li = $('ul').append(element);
  
  element.on('click', function(e) {
    let $this = $(this);
    $(this).toggleClass('done');
  });
  $('.delete').on('click', function () {
    let $this = $(this);
    $(this).closest('li').fadeOut(function () {
      $(this).closest('li').remove();
    }); 
  });

});
});
